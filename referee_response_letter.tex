\documentclass[12pt]{letter}
\usepackage{geometry}

\begin{document}
% If you want headings on subsequent pages,
% remove the ``%'' on the next line:
% \pagestyle{headings}

\begin{letter}{}
\address{}

\opening{Dear Referee,}

Please find below your concerns regarding our review paper on LQG and our response to each of the points you have raised.

1. \emph{Most importantly, in Sec. 5 the LQG variables used are typically the self-dual variables with the Barbero-Immirzi parameter set to -i.  It is true that historically these were the original variables of LQG, but since it is not known how to impose the reality conditions in the quantum theory these variables have been mostly abandoned in favour of the Ashtekar-Barbero variables where the Barbero-Immirzi parameter is taken to be real.  Therefore, while the complex-valued self-dual variables are appealing in many ways, for this paper to be a faithful introduction to LQG, I think that it is essential to focus on the case of a real-valued Barbero-Immirzi parameter (perhaps only mentioning the self-dual variables as a historical footnote).  This is especially important as the kinematical Hilbert space is known only for the case of a real $\gamma$, in particular all of the results concerning the spin-network basis have only been obtained in this case.  See for instance Sec. 4.1 in Thiemann's book Modern Canonical Quantum General Relativity and the end of Sec. IIA of Ashtekar and Lewandowski's review [2] for further details and many references concerning this point.}

While it is true that the real variables have become of central interest in recent times, the self-dual variables have great pedagogical value for explaining the steps leading to the simplified form of ADM constraints. It is with the self-dual Hamiltonian constraint the the derivation of the Kodama state, the only known exact solution of the constraints for arbitrary connections, is also made transparent.

Moreover, in recent years self-dual variables have made a comeback in work Frodden, Perez, Ghosh; Wieland; and Pranzetti.

Wieland has shown that starting from the complex variables (with SL(2,C) as gauge group) one can perform the canonical quantization procedure and obtain the \emph{same} kinematic Hilbert space as in the SU(2) case. Thus the earlier concerns regarding the viability of complex variables vis-a-vis quantization difficulties have been resolved.

FPG have shown that analytic continuation of the horizon SU(2) Chern-Simons theory to complex values of the level $ k $, leads to the asymptotic form of the Hilbert space dimension agreeing with the Bekenstein-Hawking entropy in limit of large spins.

Pranzetti has used complex $ \gamma $ to define a geometrical notion of temperature for the horizon and to calculate the Boltzmann and von-Neumann entropies for a gas of punctures leading, once again, to the BH form of the entropy in the large-spin limit.

Thus, in our humble opinion, we disagree with the referee's observation that complex variables should be consigned to a historical footnote.

Section 5.2 introduces real variables and the Barbero-Immirzi parameter and provides an extensive set of references to the discussion surrounding the meaning of this parameter.

2. \emph{It is also important to separate the canonical and covariant approaches.  In particular, I would strongly recommend explaining the ADM formalism in tetrad variables so the reader can see how we go from tetrads to triads in the canonical formalism.  It would also be helpful to clarify a number of remarks in Secs. 5 and 6 about tetrads that could otherwise be confusing to the reader even though it is the canonical framework that is under consideration.}

A section on the ADM formalism in tetrad variables has been included. In the main text (Section 4.5.4) we have followed Thiemann's approach from his Canonical Gravity book. The full treatment of the Palatini action is given in Appendix D.

3. \emph{Concerning the volume operator, perhaps the authors should also reference, in addition to the Rovelli-Smolin and Ashtekar-Lewandowski definitions, the polyhedral volume operator proposed by Bianchi, Dona and Speziale (arXiv:1009.3402); there are some interesting results in the five-valent case obtained by Haggard (arXiv:1211.7311).  It would also be helpful to point out that the three-valent vertex always has a vanishing volume, no matter the choice of the volume operator.}

The suggested references and the point about the three-valent vertex have been included as a footnote in the section on the volume operator.

4. \emph{In Sec. 7.1 on black hole entropy, there is a lot of important recent work that is not mentioned.  In particular, the work by Engle, Noui, Perez and Pranzetti (arXiv:1006.0634, arXiv:1103.2723) arguing for a SU(2) Chern-Simons theory on the isolated horizon, and suggestions by Bianchi (see http://relativity.phys.lsu.edu/ilqgs/bianchi012913.pdf and references therein) that the entropy is in fact due to entanglement and not microscopic degrees of freedom are both important results that should be explained here.  In short, the study of black hole entropy in LQG is a very rich and active field and the results presented in this review, while very important and pioneering, should not been seen as the final word on this topic.}

Section 7.1.3 has been added with references to the suggested papers on CS theory. Work on entanglement entropy by Bianchi and other authors has been elaborated on in Sec. 7.1.4 on entanglement entropy of black holes.

5. \emph{In Sec. 7.2 on loop quantum cosmology there is no mention of the quantisation of the model, or of singularity resolution.  There are already some very good reviews of loop quantum cosmology in the literature that should be referenced, including Bojowald (Living Rev.Rel. 11 (2008) 4) which is an update to [49], Ashtekar and Singh (arXiv:1108.0893), Banerjee, Calcagni and Martin-Benito (arXiv:1109.6801), and for this section to provide an adequate introduction to LQC the quantum theory should be explained in at least some detail, and it is also important to explain the main results concerning singularity resolution in a large number of cosmological space-times.}

The LQC section has been substantially expanded to include discussion of the procedure involved in going from the Hamiltonian constraint for FRW cosmology in metric variables to the loop quantized version expressed in terms of holonomies and triads. Fundamental facts about the triad operator, triad eigenstates and their properties, boundedness of volume operator etc have been explained, leading to an introduction to the singularity resolution in big bang cosmology and in the context of black holes. The suggested papers have also been referenced in the text.

6. \emph{Further, different versions of the holographic principle have been suggested in various contexts, and it is not clear which version of the holographic principle the authors are referring to in their comments.  In fact, under the argument the authors present, any quantum theory where amplitudes can be calculated from initial and final states is ``holographic''.  In general relativity, perhaps the most interesting formulation of the holographic principle is due to Bousso (hep-th/9905177), and it is not at all obvious if it would hold in LQG.  While the fact that Bousso's covariant entropy bound conjecture does hold in certain loop quantum cosmology space-times (see Ashtekar and Wilson-Ewing, arXiv:0805.3511) is encouraging, the possibility of a generalization to full LQG remains an open question.  This should at least be made clear in this tutorial.}

The caveats regarding holography in LQG have been added to the text. Bousso's bound and the Ashtekar, Wilson-Ewing paper have been mentioned in this regard.

7. \emph{Finally, the authors may reconsider the inclusion of App. I on the Kodama state since it was proposed for the case of self-dual variables (and especially since the Kodama state is not mentioned elsewhere in the paper).}

In our value the Kodama state is of great pedagogical value for students of quantum gravity. Therefore we have kept the appendix. However, an introduction and motivation for studying the Kodama state has been provided in the Sec 7.1.3 on the Chern-Simons approach to black hole entropy.

We hope that we have satisfactorily addressed all of the referee's concerns. We welcome the referee's feedback on the revised paper and would be happy to address any other concerns the referee might have.

\signature{Deepak \& Sundance}

\closing{Sincerely,}

%enclosure listing
%\encl{}

\end{letter}
\end{document}
